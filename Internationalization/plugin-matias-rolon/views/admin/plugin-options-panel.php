<?php 

?>

<div class="wrap">
    <h1><?php esc_html_e('Settings', 'yith-plugin-matias-rolon' );?></h1>

    <form method="post" action="options.php">
        <?php
            settings_fields( 'pmr-options-page' );
		    do_settings_sections( 'pmr-options-page' );
            submit_button();
        ?>

    </form>
</div>