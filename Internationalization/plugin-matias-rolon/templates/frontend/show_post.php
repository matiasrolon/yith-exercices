<?php
/*
 * This file belongs to the YITH PS Plugin Matias Rolon.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

 ?>

 <div class="yith-pmr-post-container">

    <?php

	if( 'yes' == $show_image ) {
		?>
	 	<div class="yith-pmr-post-container-image">

	 <?php

			echo get_the_post_thumbnail( $post->ID, 'thumbnail', array( 'class' => 'alignleft' ) );

		?>
	 	</div>
	 	<?php
	}

	?>

	 <div class="yith-pmr-post-content">
		 <?php echo $post->post_content; ?>
	 </div>

 </div>
