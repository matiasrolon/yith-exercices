<?php
/*
 * This file belongs to the YITH PS Plugin Testimonials.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

 ?>

 <div    class= " yith-pt-testimonial-container  <?php if($hover_effect == "zoom") { 
	 													echo 'withZoom'; 
 													}elseif ($hover_effect == "highlight"){
														echo 'withHighlight';
													} ?>">
    <?php

	if( 'yes' == $show_image ) {
		?>
	 	<div class="yith-pt-testimonial-image">
			<img src=<?php echo get_the_post_thumbnail_url($post->ID,'full'); ?> alt="">
		</div>
	 	<?php
	}
	?>
	 <div class="yith-pt-testimonial-name">
		 <?php echo get_the_title($post->ID); ?>
	 </div>
	 <div class="yith-pt-testimonial-content">
		 <?php echo $post->post_content; ?>
	 </div>

 </div>
