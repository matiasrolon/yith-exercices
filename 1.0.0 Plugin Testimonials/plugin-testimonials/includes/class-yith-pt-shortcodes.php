<?php
/*
 * This file belongs to the YITH PT Plugin Testimonials.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_PT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PT_Shortcodes' ) ) {

	class YITH_PT_Shortcodes {

        /**
		 * Main Instance
		 *
		 * @var YITH_PT_Shortcodes
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		
        /**
         * Main plugin Instance
         *
         * @return YITH_PT_Post_Types Main instance
         * @author Matias Rolon <matiasrolon.it@gmail.com>
         */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }
        
		/**
		 * YITH_PT_Post_Types constructor.
		 */
		private function __construct() {

			$shortcodes = array(
				'yith_pt_show_all_testimonials'     => __CLASS__ . '::show_all_testimonials', // print testimonials.
			);
			foreach ( $shortcodes as $shortcode => $function ) {
				add_shortcode( $shortcode, $function );
			}

		}

		/**
		 * Show all testimonials
		 *
		 * @return .
		 */
		public static function show_all_testimonials ($args){
			wp_enqueue_style('yith-pt-frontend-shortcode-css');
			extract( 
				shortcode_atts( array(  //parametro => valor por defecto
					'number' => 2, 
					'ids' => '57,59,53', //57,53,59
					'hover_effect' => '',
					'show_image' => 'yes',								
			), $args) );

			$ids_to_show = explode ( ',' , $ids);
			$posts = get_posts(array(
				'numberpost' => $number,
				'include' => $ids_to_show,
				'post_type' => 'pt-testimonials',
			));

			ob_start();

			foreach ( $posts as $post ) {
				yith_pt_get_template( '/frontend/show_testimonials.php', array(
					'post' 			  => $post,
					'show_image'      => $show_image,
					'hover_effect'    => $hover_effect,
				) );
			}
			return '<div class="yith-pt-testimonials"> ' . ob_get_clean() . '</div>';           
		}

	}	
}