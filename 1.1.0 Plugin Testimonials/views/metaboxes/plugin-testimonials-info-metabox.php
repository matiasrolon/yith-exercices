<?php

?>

<div class="ga-pt-form">  
    <div class="ga-pt-form__input ga-pt-form__input-role">
        <label class="ga-pt-label ga-pt-form__input-role__label" for="pt_role"><?php _e( 'Role', 'yith-plugin-testimonials' ); ?></label>
        <select class="ga-pt-form__input-role__select" name="_yith_pt_role" id="pt_role">  
            <option value="admin" <?php selected( esc_html( get_post_meta( $post->ID, '_yith_pt_role', true ) ), 'admin' ); ?>>Admin</option>  
            <option value="user" <?php selected( esc_html( get_post_meta( $post->ID, '_yith_pt_role', true ) ), 'user' ); ?>>User</option>  
        </select>
    </div>   

    <div class="ga-pt-form__input ga-pt-form__input-company">
        <label class="ga-pt-label ga-pt-form__input-company__label" for="pt_company"><?php _e( 'company', 'yith-plugin-testimonials' ); ?></label>
        <input type="text" class="ga-pt-form__input-company__input" name="_yith_pt_company" id="pt_company"
               value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_company', true ) ); ?>">
    </div>  

    <div class="ga-pt-form__input ga-pt-form__input-web_company">
        <label class="ga-pt-label ga-pt-form__input-web_company__label" for="pt_web_company"><?php _e( 'web company', 'yith-plugin-testimonials' ); ?></label>
        <input type="url" class="ga-pt-form__input-web_company__input" name="_yith_pt_web_company" id="pt_web_company"
               value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_web_company', true ) ); ?>">
    </div>  

    <div class="ga-pt-form__input ga-pt-form__input-email">
        <label class="ga-pt-label ga-pt-form__input-email__label" for="pt_email"><?php _e( 'Email', 'yith-plugin-testimonials' ); ?></label>
        <input type="email" class="ga-pt-form__input-email__input" name="_yith_pt_email" id="pt_email"
               value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_email', true ) ); ?>">
    </div>  

    <div class="ga-pt-form__input ga-pt-form__input-rating">
        <label class="ga-pt-label ga-pt-form__input-rating__label" for="pt_rating"><?php _e( 'Rating', 'yith-plugin-testimonials' ); ?></label>
        <input type="number" min="0" max="5" class="ga-pt-form__input-rating__input" name="_yith_pt_rating" id="pt_rating"
               value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_rating', '1' ) ); ?>">
    </div>  

    <div class="ga-pt-form__input ga-pt-form__input-vip">
        <label class="ga-pt-label ga-pt-form__input-vip__label" for="pt_vip"><?php _e( 'VIP', 'yith-plugin-testimonials' ); ?></label>
        <input type="checkbox" class="ga-pt-form__input-vip__input" name="_yith_pt_vip"  id="pt_vip"
             value="<?php checked( get_post_meta( $post->ID, '_yith_pt_vip', 'yes' ), 'yes' ); ?>"
        >
    </div>  

    <div class="ga-pt-badge-informacion"> 
        <div class="ga-pt-form__input ga-pt-form__input-badge">
            <label class="ga-pt-label ga-pt-form__input-badge__label" for="pt_badge"><?php _e( 'Enable Badge', 'yith-plugin-testimonials' ); ?></label>
            <input type="checkbox" class="ga-pt-form__input-badge__input" name="_yith_pt_badge"  id="pt_badge"
                   value="<?php checked( get_post_meta( $post->ID, '_yith_pt_badge', 'yes' ), 'yes' ); ?>"
            >
        </div>  
        <div class="ga-pt-form__input ga-pt-form__input-badge_text">
            <label class="ga-pt-label ga-pt-form__input-badge_text__label" for="pt_badge_text"><?php _e( 'Badge text', 'yith-plugin-testimonials' ); ?></label>
            <input type="text" class="ga-pt-form__input-badge_text__input" name="_yith_pt_badge_text" id="pt_badge_text"
                    value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_badge_text', true ) ); ?>">
        </div>  

        <div class="ga-pt-form__input ga-pt-form__input-badge_color">
            <label class="ga-pt-label ga-pt-form__input-badge_color__label" for="pt_badge_color"><?php _e( 'Badge background color', 'yith-plugin-testimonials' ); ?></label>
            <input type="text" class="ga-pt-form__input-badge_color__input" name="_yith_pt_badge_color" id="pt_badge_color"
                    value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_badge_color', true ) ); ?>">
        </div>        
    </div>
</div>