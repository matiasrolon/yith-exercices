<?php
/*
 * This file belongs to the YITH PT Plugin Testimonials.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_PT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PT_Admin' ) ) {

	class YITH_PT_Admin {

        /**
		 * Main Instance
		 *
		 * @var YITH_PT_Admin
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
        
        /**
         * Main plugin Instance
         *
         * @return YITH_PT_Admin Main instance
         * @author Matias Rolon <matiasrolon.it@gmail.com>
         */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }
        
		/**
		 * YITH_PT_Admin constructor.
		 */
		private function __construct() {

			add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
			add_action( 'save_post', array( $this, 'save_meta_box' ) );

		}


		/**
		 * Setup the meta boxes
		 */
		public function add_meta_boxes() {
			add_meta_box( 'yith-pt-additional-information', __( 'Additional information', 'yith-plugin-testimonials' ), array(
				$this,
				'view_meta_boxes'
			), YITH_PT_Post_Types::$post_type );
		}

		/**
		 * Wiev meta boxes
		 *
		 * @param $post
		 */
		public function view_meta_boxes( $post ) {
			yith_pt_get_view( '/metaboxes/plugin-testimonials-info-metabox.php', array( 'post' => $post) );
		}

		/**
		 * Save meta box values
		 *
		 * @param $post_id
		 */
		public function save_meta_box( $post_id ) {

			if ( YITH_PT_Post_Types::$post_type !== get_post_type( $post_id ) ) {
				return;
			}
			if ( isset( $_POST[ '_yith_pt_role' ] ) ) {
				update_post_meta( $post_id, '_yith_pt_role', $_POST[ '_yith_pt_role' ] );
			}
			if ( isset( $_POST[ '_yith_pt_company' ] ) ) {
				update_post_meta( $post_id, '_yith_pt_company', $_POST[ '_yith_pt_company' ] );
			}
			if ( isset( $_POST[ '_yith_pt_web_company' ] ) ) {
				update_post_meta( $post_id, '_yith_pt_web_company', $_POST[ '_yith_pt_web_company' ] );
			}
			if ( isset( $_POST[ '_yith_pt_email' ] ) ) {
				update_post_meta( $post_id, '_yith_pt_email', $_POST[ '_yith_pt_email' ] );
			}
			if ( isset( $_POST[ '_yith_pt_rating' ] ) ) {
				update_post_meta( $post_id, '_yith_pt_rating', $_POST[ '_yith_pt_rating' ] );
			}
			if ( isset( $_POST[ '_yith_pt_vip' ] ) ) {
				$value =  ($_POST[ '_yith_pt_vip' ] == 'yes') ? 'yes' : 'no';
				update_post_meta( $post_id, '_yith_pt_vip', $value);
			}
			if ( isset( $_POST[ '_yith_pt_badge' ] ) ) {
				$value =  ($_POST[ '_yith_pt_badge' ] == 'yes') ? 'yes' : 'no';
				update_post_meta( $post_id, '_yith_pt_badge', $value);
			}
			if ( isset( $_POST[ '_yith_pt_badge_text' ] ) ) {
				update_post_meta( $post_id, '_yith_pt_badge_text', $_POST[ '_yith_pt_badge_text' ] );
			}
			if ( isset( $_POST[ '_yith_pt_badge_color' ] ) ) {
				update_post_meta( $post_id, '_yith_pt_badge_color', $_POST[ '_yith_pt_badge_color' ] );
			}

		}

	}	
}