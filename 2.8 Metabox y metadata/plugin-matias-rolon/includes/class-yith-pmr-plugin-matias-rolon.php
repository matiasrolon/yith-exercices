<?php
/*
 * This file belongs to the YITH PMR Plugin Matias_Rolon.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_PMR_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PMR_Plugin_Matias_Rolon' ) ) {
	class YITH_PMR_Plugin_Matias_Rolon {

        /**
		 * Main Instance
		 *
		 * @var YITH_PMR_Plugin_Matias_Rolon
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
        /**
		 * Main Admin Instance
		 *
		 * @var YITH_PMR_Plugin_Matias_Rolon_Admin
		 * @since 1.0
		 */
		public $admin = null;
		/**
		 * Main Frontpage Instance
		 *
		 * @var YITH_PMR_Plugin_Matias_Rolon_Frontend
		 * @since 1.0
		 */
        public $frontend = null;

		/**
		 * Main Shortcodes Instance
		 * @var YITH_PMR_Plugin_Matias_Rolon_Frontend
		 * @since 1.0
		 */
        public $shortcodes = null;

        /**
         * Main plugin Instance
         *
         * @return YITH_PMR_Plugin_Matias_Rolon Main instance
         * @author Carlos Rodríguez <carlos.rodriguez@yithemes.com>
         */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();

			// ternary operator --> https://www.codementor.io/@sayantinideb/ternary-operator-in-php-how-to-use-the-php-ternary-operator-x0ubd3po6 
        }
        
		/**
		 * YITH_PMR_Plugin_Matias_Rolon constructor.
		 */
		private function __construct() {


            $require = apply_filters('yith_pmr_require_class',
				 array(
					'common'   => array(
						'includes/class-yith-pmr-post-types.php',
						'includes/functions.php',
						'includes/class-yith-pmr-shortcodes.php'
						//'includes/class-yith-pmr-ajax.php',
						//'includes/class-yith-pmr-compatibility.php',
						//'includes/class-yith-pmr-other-class.php',
					),
					'admin' => array(
						'includes/class-yith-pmr-admin.php',
					),
					'frontend' => array(
						'includes/class-yith-pmr-frontend.php',
					),
				)
			);

			$this->_require($require);

			$this->init_classes();

			/* 
				Here set any other hooks ( actions or filters you'll use on this class)
			*/
			
			// Finally call the init function
			$this->init();

		
		}
	
		/**
		 * Add the main classes file
		 *
		 * Include the admin and frontend classes
		 *
		 * @param $main_classes array The require classes file path
		 *
		 * @author Carlos Rodríguez <carlos.rodriguez@yithemes.com>
		 * @since  1.0
		 *
		 * @return void
		 * @access protected
		 */
		protected function _require($main_classes)
		{
			foreach ($main_classes as $section => $classes) {
				foreach ($classes as $class) {
					if ('common' == $section || ('frontend' == $section && !is_admin() || (defined('DOING_AJAX') && DOING_AJAX)) || ('admin' == $section && is_admin()) && file_exists(YITH_PMR_DIR_PATH . $class)) {
						require_once(YITH_PMR_DIR_PATH . $class);
					}
				}
			}
		}

		/**
		 * Init common class if they are necessary
		 * @author Carlos Rodríguez <carlos.rodriguez@yithemes.com>
		 * @since  1.0
		 **/
		public function init_classes(){
			//$this->function = YITH_PMR_Other_Class::get_instance();
			//$this->ajax = YITH_PMR_Ajax::get_instance();
			//$this->compatibility = YITH_PMR_Compatibility::get_instance();
			$this->shortcodes = YITH_PMR_Shortcodes::get_instance();
			YITH_PMR_Post_Types::get_instance();
		}

		/**
         * Function init()
         *
         * Instance the admin or frontend classes
         *
         * @author Carlos Rodríguez <carlos.rodriguez@yithemes.com>
         * @since  1.0
         * @return void
         * @access protected
         */
        public function init()
        {
            if (is_admin()) {
                $this->admin =  YITH_PMR_Admin::get_instance();
            }

            if (!is_admin() || (defined('DOING_AJAX') && DOING_AJAX)) {
                $this->frontend = YITH_PMR_Frontend::get_instance();
            }
        }

	}	
}
/**
 * Get the YITH_PMR_Plugin_Matias_Rolon instance
 *
 * @return YITH_PMR_Plugin_Matias_Rolon
 */
if ( ! function_exists( 'yith_pmr_plugin_matias_rolon' ) ) {
	function yith_pmr_plugin_matias_rolon() {
		return YITH_PMR_Plugin_Matias_Rolon::get_instance();
	}
}