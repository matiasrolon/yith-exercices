<?php
/*
 * This file belongs to the YITH PMR Plugin Matias Rolon.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_PMR_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PMR_Shortcodes' ) ) {

	class YITH_PMR_Shortcodes {

        /**
		 * Main Instance
		 *
		 * @var YITH_PMR_Shortcodes
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		
        /**
         * Main plugin Instance
         *
         * @return YITH_PMR_Post_Types Main instance
         * @author Carlos Rodríguez <carlos.rodriguez@yithemes.com>
         */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }
        
		/**
		 * YITH_PMR_Post_Types constructor.
		 */
		private function __construct() {

			$shortcodes = array(
				'yith_pmr_show_post_type'       => __CLASS__ . '::show_post_types', // print post.
				'yith_pmr_show_all_books'       => __CLASS__ . '::show_all_books', // print books.
			);
			foreach ( $shortcodes as $shortcode => $function ) {
				add_shortcode( $shortcode, $function );
			}

		}

		public static function show_post_types( $args ) {

			//Enqueue custom CSS for the shortcode
			wp_enqueue_style('yith-pmr-frontend-shortcode-css');

			$args = shortcode_atts( array(
										'numberpost' => get_option( 'yith_pmr_shortcode_number', 6 ),
										'show_image' => get_option( 'yith_pmr_shortcode_show_image', 'no' ),
										'post_type' => 'pmr-book',
									), $args,'yith_auction_products' );

			$posts = get_posts($args);


			ob_start();

			foreach ( $posts as $post ) {
				yith_pmr_get_template( '/frontend/show_post.php', array(
					'post' 			  => $post,
					'show_image'      => $args['show_image'],
				) );
			}


			return '<div class="yith-pmr-posts"> ' . ob_get_clean() . '</div>';


		}

		/**
		 * Show all books
		 *
		 * @return .
		 */
		public static function show_all_books (){
			$args = array(
							'post_type'      => 'pmr-book',
							'posts_per_page' => get_option( 'yith_pmr_shortcode_number', 5 ),
						);
		
			$query = new WP_Query($args);
			$result = "";
			if($query->have_posts()) :
				
				while($query->have_posts()) :
					
					$query->the_post() ;
					
					$image ='';
					if (get_option( 'yith_pmr_shortcode_show_image', 'yes' ) == 'yes'):
						$image = '<img src="' . get_the_post_thumbnail_url(get_the_ID(),'full') . '">';
					endif;

					$result .= '<div class="book-container">
									<h3>' .  
										get_the_title() .
									'</h3>' .			
										$image . 
									'<p> 
										<label> ISBN: </label>' .  
										get_post_meta( get_the_ID(), '_yith_pmr_ISBN', true ) .
									'</p>
									<p> 
										<label> Price: </label>' .  
										get_post_meta( get_the_ID(), '_yith_pmr_price', true ) .
									'</p>					
					 				<p> 
									 	<label> Cover type: </label>' .  
										 get_post_meta( get_the_ID(), '_yith_pmr_cover_type', true ) .
									'</p>
									<p> 
										<label> Language: </label>' .  
										get_post_meta( get_the_ID(), '_yith_pmr_language', true ) .
							   		'</p>
								 </div>';
				endwhile;
				
				wp_reset_postdata();
		
			endif;    

			return $result;            
		}

	}	
}