<?php

?>

<div class="ga-tst-form">
    <div class="ga-tst-form__input ga-tst-form__input-price">
        <label class="ga-tst-form__input-price__label" for="tst_price"><?php _e( 'Price', 'yith-plugin-matias-rolon' ); ?></label>
        <input type="number" class="ga-tst-form__input-price__input" name="_yith_pmr_price" id="tst_price"
               value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pmr_price', true ) ); ?>">
    </div>    
    <div class="ga-tst-form__input ga-tst-form__input-ISBN">
        <label class="ga-tst-form__input-ISBN__label" for="tst_ISBN"><?php _e( 'ISBN', 'yith-plugin-matias-rolon' ); ?></label>
        <input type="text" class="ga-tst-form__input-ISBN__input" name="_yith_pmr_ISBN" id="tst_ISBN"
               value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pmr_ISBN', true ) ); ?>">
    </div>    
    <div class="ga-tst-form__input ga-tst-form__input-cover_type">
        <label class="ga-tst-form__input-cover_type__label" for="tst_cover_type"><?php _e( 'Cover type', 'yith-plugin-matias-rolon' ); ?></label>
        <select class="ga-tst-form__input-cover_type__select" name="_yith_pmr_cover_type" id="tst_cover_type">  
            <option value="dura" <?php selected( esc_html( get_post_meta( $post->ID, '_yith_pmr_cover_type', true ) ), 'dura' ); ?>>Dura</option>  
            <option value="blanda" <?php selected( esc_html( get_post_meta( $post->ID, '_yith_pmr_cover_type', true ) ), 'blanda' ); ?>>Blanda</option>  
        </select>
    </div>    
        
    <div class="ga-tst-form__input ga-tst-form__input-language">
        <label class="ga-tst-form__input-language__label" for="tst_language"><?php _e( 'Language', 'yith-plugin-matias-rolon' ); ?></label>
        <input type="text" class="ga-tst-form__input-language__input" name="_yith_pmr_language" id="tst_language"
               value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pmr_language', true ) ); ?>">
    </div>   
</div>