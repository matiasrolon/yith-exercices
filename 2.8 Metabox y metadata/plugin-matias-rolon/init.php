<?php

/*
 * Plugin Name: YITH Plugin Matias Rolon
 * Description: Skeleton for YITH Matias Rolon
 * Version: 1.0.0
 * Author: Matias Rolon
 * Author URI: https://yithemes.com/
 * Text Domain: yith-plugin-matias-rolon
 */

! defined( 'ABSPATH' ) && exit;   //Before all, check if defined ABSPATH.

/*Create some constant where defined PATH for Style, Assets, Templates, Views */

if ( ! defined( 'YITH_PMR_VERSION' ) ) {
	define( 'YITH_PMR_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_PMR_DIR_URL' ) ) {
	define( 'YITH_PMR_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_PMR_DIR_ASSETS_URL' ) ) {
	define( 'YITH_PMR_DIR_ASSETS_URL', YITH_PMR_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_PMR_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_PMR_DIR_ASSETS_CSS_URL', YITH_PMR_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_PMR_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_PMR_DIR_ASSETS_JS_URL', YITH_PMR_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_PMR_DIR_PATH' ) ) {
	define( 'YITH_PMR_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_PMR_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_PMR_DIR_INCLUDES_PATH', YITH_PMR_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_PMR_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_PMR_DIR_TEMPLATES_PATH', YITH_PMR_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_PMR_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_PMR_DIR_VIEWS_PATH', YITH_PMR_DIR_PATH . 'views' );
}

// Different way to declare a constant

! defined( 'YITH_PMR_INIT' )               && define( 'YITH_PMR_INIT', plugin_basename( __FILE__ ) );
! defined( 'YITH_PMR_SLUG' )               && define( 'YITH_PMR_SLUG', 'yith-plugin-matias-rolon' );
! defined( 'YITH_PMR_SECRETKEY' )          && define( 'YITH_PMR_SECRETKEY', 'zd9egFgFdF1D8Azh2ifA' );
! defined( 'YITH_PMR_OPTIONS_PATH' )       && define( 'YITH_PMR_OPTIONS_PATH', YITH_PMR_DIR_PATH . 'plugin-options' );

/**
 * Include the scripts
 */
if ( ! function_exists( 'yith_pmr_init_classes' ) ) {

	function yith_pmr_init_classes() {

		load_plugin_textdomain( 'yith-plugin-matias-rolon', false, basename( dirname( __FILE__ ) ) . '/languages' );

		//Require all the files you include on your plugins. Example
		require_once YITH_PMR_DIR_INCLUDES_PATH . '/class-yith-pmr-plugin-matias-rolon.php';

		if ( class_exists( 'YITH_PMR_Plugin_Matias_Rolon' ) ) {
			/*
			*	Call the main function
			*/
			yith_pmr_plugin_matias_rolon();
		}
	}
}


add_action( 'plugins_loaded', 'yith_pmr_init_classes', 11 );




