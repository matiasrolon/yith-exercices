<?php
/*
 * This file belongs to the YITH PMR Plugin Matias Rolon.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_PMR_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PMR_Post_Types' ) ) {

	class YITH_PMR_Post_Types {

        /**
		 * Main Instance
		 *
		 * @var YITH_PMR_Post_Types
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Post type name
		 *
		 * @var YITH_PMR_Post_Types
		 * @since 1.0
		 * @access public
		 */
		public static $post_type = 'pmr-book';

		
        /**
         * Main plugin Instance
         *
         * @return YITH_PMR_Post_Types Main instance
         * @author Carlos Rodríguez <carlos.rodriguez@yithemes.com>
         */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }
        
		/**
		 * YITH_PMR_Post_Types constructor.
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'setup_post_type' ) );
			add_action('init',array( $this ,'register_taxonomy'));

		}

		/**
		 * Setup the 'Skeleton' custom post type
		 */
		public function setup_post_type() {
			$labels = array(
				'name'               => _x( 'Books', 'post type general name', 'yith-plugin-matias-rolon' ),
				'singular_name'      => _x( 'Book', 'post type singular name', 'yith-plugin-matias-rolon' ),
				'menu_name'          => _x( 'Books', 'admin menu', 'yith-plugin-matias-rolon' ),
				'add_new'            => _x( 'Add new', 'book', 'yith-plugin-matias-rolon' ),
				'add_new_item'       => __( 'Add new book', 'yith-plugin-matias-rolon' ),
				'new_item'           => __( 'New book', 'yith-plugin-matias-rolon' ),
				'edit_item'          => __( 'Edit book', 'yith-plugin-matias-rolon' ),
				'view_item'          => __( 'View book', 'yith-plugin-matias-rolon' ),
				'all_items'          => __( 'All books', 'yith-plugin-matias-rolon' ),
				'search_items'       => __( 'Search books', 'yith-plugin-matias-rolon' ),
				'not_found'          => __( 'Not books found .', 'yith-plugin-matias-rolon' ),
				'not_found_in_trash' => __( 'Not books found in trash.', 'yith-plugin-matias-rolon' )
			);
			$args = array(
				'labels'        => $labels,
				'description'  =>  __('Book post type','yith-plugin-matias-rolon'),
				'public'       => false,
				'menu_icon'    => 'dashicons-universal-access',
				'show_in_menu' => true,
				'show_ui'      => true,
				'rewrite'      => false,
				'supports'     => array( 'title', 'editor', 'author', 'thumbnail' ),
			);
			register_post_type( self::$post_type, $args );
		}

		public function register_taxonomy() {
			  // Add new taxonomy, make it hierarchical (like categories)
			  $labels = array(
				'name'              => _x( 'Generos', 'taxonomy general name', 'yith-plugin-matias-rolon' ),
				'singular_name'     => _x( 'Genre', 'taxonomy singular name', 'yith-plugin-matias-rolon' ),
				'search_items'      => __( 'Search genre', 'yith-plugin-matias-rolon' ),
				'all_items'         => __( 'All genres', 'yith-plugin-matias-rolon' ),
				'parent_item'       => __( 'Parent genre', 'yith-plugin-matias-rolon' ),
				'parent_item_colon' => __( 'Parent genre:', 'yith-plugin-matias-rolon' ),
				'edit_item'         => __( 'Edit genre', 'yith-plugin-matias-rolon' ),
				'update_item'       => __( 'Update genre', 'yith-plugin-matias-rolon' ),
				'add_new_item'      => __( 'Add new genre', 'yith-plugin-matias-rolon' ),
				'new_item_name'     => __( 'New genre name', 'yith-plugin-matias-rolon' ),
				'menu_name'         => __( 'Genres', 'yith-plugin-matias-rolon' ),
			);
		 
			$args = array(
				'hierarchical'      => true,
				'labels'            => $labels,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'yith_tax_hierarchical' ),
			);
		 
			register_taxonomy( 'yith_pmr_book_tax', array( self::$post_type ), $args );

		}

	}	
}