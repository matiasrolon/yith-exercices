<?php
/*
 * This file belongs to the YITH PMR Plugin Matias Rolon.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_PMR_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PMR_Shortcodes' ) ) {

	class YITH_PMR_Shortcodes {

        /**
		 * Main Instance
		 *
		 * @var YITH_PMR_Shortcodes
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		
        /**
         * Main plugin Instance
         *
         * @return YITH_PMR_Post_Types Main instance
         * @author Carlos Rodríguez <carlos.rodriguez@yithemes.com>
         */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }
        
		/**
		 * YITH_PMR_Post_Types constructor.
		 */
		private function __construct() {

			$shortcodes = array(
				'yith_pmr_show_post_type'       => __CLASS__ . '::show_post_types', // print post.
			);
			foreach ( $shortcodes as $shortcode => $function ) {
				add_shortcode( $shortcode, $function );
			}

		}

		public static function show_post_types( $args ) {

			//Enqueue custom CSS for the shortcode
			wp_enqueue_style('yith-pmr-frontend-shortcode-css');

			$args = shortcode_atts( array(
										'numberpost' => get_option( 'yith_pmr_shortcode_number', 6 ),
										'show_image' => get_option( 'yith_pmr_shortcode_show_image', 'no' ),
										'post_type' => 'pmr-matias-rolon',
									), $args,'yith_auction_products' );

			$posts = get_posts($args);


			ob_start();

			foreach ( $posts as $post ) {
				yith_pmr_get_template( '/frontend/show_post.php', array(
					'post' 			  => $post,
					'show_image'      => $args['show_image'],
				) );
			}


			return '<div class="yith-pmr-posts">' . ob_get_clean() . '</div>';


		}



	}	
}