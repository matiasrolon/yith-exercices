<?php
/*
 * This file belongs to the YITH PMR PlugiN Matias Rolon.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_PMR_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PMR_Frontend' ) ) {

	class YITH_PMR_Frontend {

        /**
		 * Main Instance
		 *
		 * @var YITH_PMR_Frontend
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
        
        /**
         * Main plugin Instance
         *
         * @return YITH_PMR_Frontend Main instance
         * @author Carlos Rodríguez <carlos.rodriguez@yithemes.com>
         */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }
        
		/**
		 * YITH_PMR_Frotend constructor.
		 */
		private function __construct() {

			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

		}

		public function enqueue_scripts() {
			wp_register_style( 'yith-pmr-frontend-shortcode-css', YITH_PMR_DIR_ASSETS_CSS_URL . '/shortcode.css',array(),YITH_PMR_VERSION );

		}

	}	
}