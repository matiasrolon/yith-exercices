<?php
/*
 * This file belongs to the YITH PMR Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_PMR_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PMR_Admin' ) ) {

	class YITH_PMR_Admin {

        /**
		 * Main Instance
		 *
		 * @var YITH_PMR_Admin
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
        
        /**
         * Main plugin Instance
         *
         * @return YITH_PMR_Admin Main instance
         * @author Carlos Rodríguez <carlos.rodriguez@yithemes.com>
         */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }
        
		/**
		 * YITH_PMR_Admin constructor.
		 */
		private function __construct() {

			add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
			add_action( 'save_post', array( $this, 'save_meta_box' ) );

			// See definition on class-wp-post-list-table.php  manage_{$post_type}_posts_columns;
			add_filter('manage_pmr-matias_rolon_posts_columns', array( $this, 'add_matias_rolon_post_type_columns' ) );

			//See definition on class-wp-post-list-table.php manage_{$post->post_type}_posts_custom_column
			add_action('manage_pmr-matias_rolon_posts_custom_column', array( $this, 'display_matias_rolon_post_type_custom_column'), 10, 2 );

			// Admin menu

			add_action('admin_menu', array( $this, 'create_menu_for_general_options') );
			add_action( 'admin_init', array( $this, 'register_settings' ) );


		}


		/**
		 * Setup the meta boxes
		 */
		public function add_meta_boxes() {
			add_meta_box( 'yith-pmr-additional-information', __( 'Additional information', 'yith-plugin-matias-rolon' ), array(
				$this,
				'view_meta_boxes'
			), YITH_PMR_Post_Types::$post_type );
		}

		/**
		 * Wiev meta boxes
		 *
		 * @param $post
		 */
		public function view_meta_boxes( $post ) {
			yith_pmr_get_view( '/metaboxes/plugin-matias-rolon-info-metabox.php', array( 'post' => $post) );
		}

		/**
		 * Save meta box values
		 *
		 * @param $post_id
		 */
		public function save_meta_box( $post_id ) {

			if ( YITH_PMR_Post_Types::$post_type !== get_post_type( $post_id ) ) {
				return;
			}

			if ( isset( $_POST[ '_yith_pmr_role' ] ) ) {
				update_post_meta( $post_id, '_yith_pmr_role', $_POST[ '_yith_pmr_role' ] );
			}

		}
		/**
		 * Filters the columns displayed in the Posts list table for plugin matias_rolon post type.
		 *
		 * @param string[] $post_columns An associative array of column headings.
		 */
		public function add_matias_rolon_post_type_columns( $post_columns ) {

			$new_columns = apply_filters('yith_pmr_matias_rolon_custom_columns ', array(
				'column1' => esc_html__('Column1','yith-plugin-matias-rolon'),
				'column2' => esc_html__('Column2','yith-plugin-matias-rolon'),
			) );

			$post_columns = array_merge( $post_columns, $new_columns );

			return $post_columns;
		}
		/**
		 * Fires for each custom column of a specific post type in the Posts list table.
		 *
		 * @param string $column_name The name of the column to display.
		 * @param int    $post_id     The current post ID.
		 * */
		public function display_matias_rolon_post_type_custom_column( $column_name, $post_id ) {

			switch ( $column_name ) {

				case 'column1' :
					
						//Operations for get information for column1 example

						//$value_custom_2 = get_post_meta($post_id,'_meta_column1',true);
						//echo $value_custom_1;

						echo 'value column1 for post '. $post_id ;

					break;
				case 'column2' :

						//Operations for get information for column2 example

						//$value_custom_2 = get_post_meta($post_id,'_meta_column2',true);
						//echo $value_custom_2;

						echo 'value column2 for post '. $post_id ;
					break;
				default  : do_action('yith_pmr_matias_rolon_display_custom_column',$column,$post_id);
					break;
			}

		}
		/**
		 *  Create menu for general options
		 */
		public function create_menu_for_general_options() {

			// See the following option httpmr://developer.wordpress.org/reference/functions/add_menu_page/
				add_menu_page( 
					esc_html__( 'Plugin Skeleton Options', 'yith-plugin-matias-rolon' ),
					esc_html__( 'Plugin Skeleton Options', 'yith-plugin-matias-rolon' ),
					'manage_options',
					'plugin_matias_rolon_options',
					array($this,'matias_rolon_custom_menu_page'),
					'',
					40
				); 

		}
		/**
		 *  Callback custom menu page
		 */
		function matias_rolon_custom_menu_page() {
			yith_pmr_get_view('/admin/plugin-options-panel.php', array() );
		}

		/**
		 * Add the fields in the shortcode attribute management page
		 */

		public function register_settings() {
			
			$page_name    = 'pmr-options-page';
			$section_name = 'options_section';

			$setting_fields = array(
				array(
					'id'       => 'yith_pmr_shortcode_number',
					'title'    => esc_html__( 'Number', 'yith-plugin-matias-rolon' ),
					'callback' => 'print_number_input'
				),
				array(
					'id'       => 'yith_pmr_shortcode_show_image',
					'title'    => esc_html__( 'Show image', 'yith-plugin-matias-rolon' ),
					'callback' => 'print_show_image'
				),
			);

			add_settings_section(
				$section_name,
				esc_html__( 'Section', 'yith-plugin-matias-rolon' ),
				'',
				$page_name
			);

			foreach ( $setting_fields as $field ) {
				extract( $field );

				add_settings_field(
					$id,
					$title,
					array( $this, $callback ),
					$page_name,
					$section_name,
					array( 'label_for' => $id )
				);

				register_setting( $page_name, $id );
			}


		}

		/**
		 * Print the number input field
		 */
		public
		function print_number_input() {
			$tst_number = intval( get_option( 'yith_pmr_shortcode_number', 6 ) );
			?>
            <input type="number" id="yith_pmr_shortcode_number" name="yith_pmr_shortcode_number"
                   value="<?php echo '' !== $tst_number ? $tst_number : 6; ?>">
			<?php
		}

		/**
		 * Print the show image toggle field
		 */
		public
		function print_show_image() {
			?>
            <input type="checkbox" class="pmr-tst-option-panel__onoff__input" name="yith_pmr_shortcode_show_image"
                   value='yes'
                   id="yith_pmr_shortcode_show_image"
				<?php checked( get_option( 'yith_pmr_shortcode_show_image', '' ), 'yes' ); ?>
            >
            <label for="shortcode_show_image" class="pmr-tst-option-panel__onoff__label ">
                <span class="pmr-tst-option-panel__onoff__btn"></span>
            </label>
			<?php
		}


	
	}	
}